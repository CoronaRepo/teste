import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { RegisterService } from './services/RegisterService'

import  logoADP  from './images/logoADP.png';

import {Menubar} from 'primereact/menubar';
import {Dropdown} from 'primereact/dropdown';
import { useState } from 'react';
import { Button } from 'primereact/button';
import {InputText} from 'primereact/inputtext';
import {Panel} from 'primereact/panel';
import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import 'primeflex/primeflex.css';

class Register extends React.Component {
    constructor(){
        super();
        this.state = {
        	visible:false,
        	user:{
        		id:null,
        		username:null,
        		email:null,
        		password:null
        		},
    		selectedUser : {
            }
        }
        this.items = [
            {
                label:'User Board',
            },
            {
                label:'Admin Board'
            }
        ]
        this.registerservice = new RegisterService();
    };


    render() {
    return  <div className="p-grid p-dir-col">
                <div className="p-col-2 p-col-align-end" >
                    <img  style={{width:'50%',marginTop:'20px'}} src={logoADP} alt="logo"/>
                </div>
                <div style={{width:'80%', marginTop:'4%', margin:'0 auto'}}>
                    <Menubar  model={this.items} style={{border:'0px', fontSize: '100%', paddingLeft: '50%', background: '#d3d3d3'}}>
                        <div style={{marginTop:'1%'}}>
                            <Button label="Logout" icon="pi pi-power-off" className="p-button-secondary" style={{marginLeft:4}}/>
                        </div>
                    </Menubar>
                </div>
                <div style={{width:'25%', margin:'0 auto', marginTop:'50px'}}>
                    <Panel header="User Registration" style={{textAlign:'center'}}>
                    <br/>
                    <div className="p-grid p-fluid" >
                        <div className="p-col-12 p-md-5"  >
                           <div className="p-inputgroup" style={{width:'250%'}}>
                               <span className="p-inputgroup-addon">
                                   <i className="pi pi-user"></i>
                               </span>
                                   <InputText placeholder="Username" value={this.state.user.username} id="username" onChange={(e) => {
                                     let val = e.target.value;
                                     this.setState(prevState => {
                                     console.log(val);
                                     let user = Object.assign({}, prevState.user);
                                     user.username = val

                                     return {user};
                                 })}
                                 }/>
                           </div>
                        </div>
                    </div>

                    <div className="p-grid p-fluid"  >
                        <div className="p-col-12 p-md-5">
                            <div className="p-inputgroup" style={{width:'250%'}}>
                                <span className="p-inputgroup-addon">
                                    <i className="pi pi-envelope"></i>
                                </span>
                            <InputText placeholder="E-mail" value={this.state.user.email}  id="email" onChange={(e) => {
                                let val = e.target.value;
                                this.setState(prevState => {
                                console.log(val);
                                let user = Object.assign({}, prevState.user);
                                user.email = val

                                return {user};
                            })}
                            } />
                            </div>
                       </div>
                    </div>

                    <div className="p-grid p-fluid"  >
                        <div className="p-col-12 p-md-5">
                            <div className="p-inputgroup" style={{width:'250%'}}>
                                <span className="p-inputgroup-addon">
                                    <i className="pi pi-lock"></i>
                                </span>
                            <InputText placeholder="Password" value={this.state.user.password} id="password" onChange={(e) => {
                                  let val = e.target.value;
                                  this.setState(prevState => {
                                  console.log(val);
                                  let user = Object.assign({}, prevState.user);
                                  user.password = val

                                  return {user};
                              })}
                              } />
                            </div>
                        </div>
                    </div>

                    <br/>
                    <Button label="Register" className="p-button-rounded" style={{width:'40%'}} />
                    </Panel>
                </div>
            </div>
    }
}
export default Register;