import axios from 'axios';

export class RegisterService{
    baseUrl = "http://localhost:8080/api/";

    getAll(){
        return axios.get(this.baseUrl + "register").then(res => res.data);
    }

    save(user) {
        	return axios.post(this.baseUrl + "register/save", user).then(res => res.data);
        }
}